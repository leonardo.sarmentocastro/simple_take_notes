// export reducer as default
import reducer from './reducer';
export default reducer;

// name export everything else
export { default as kanbanCardsTypes } from './types';
export { default as kanbanCardsActions } from './actions';
export * from './selectors';
