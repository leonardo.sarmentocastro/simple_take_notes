import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

import { KanbanCardContainer } from '../';
import './KanbanColumn.styles.scss';

export default class KanbanColumn extends Component {
  whenCardIsDraggedToColumn = (event) => {
    event.preventDefault();

    // read "kanbanCard" data from browser data transfer layer
    const data = event.dataTransfer.getData('text');
    const draggingKanbanCard = JSON.parse(data);

    // only update the store once a card is dragged from column A-to-B, not A-A
    const fromKanbanColumnId = (draggingKanbanCard.kanbanColumn);
    const toKanbanColumnId = (this.props.kanbanColumn.id);
    const isDraggingCardToDifferentColumn = (fromKanbanColumnId !== toKanbanColumnId);
    if (isDraggingCardToDifferentColumn) {
      this.props.dragKanbanCard(draggingKanbanCard, toKanbanColumnId);
    }
  }

  setElementAsDropzone = (event) => event.preventDefault();

  get cards() { return this.props.kanbanColumn.cards; }
  get hasCards() { return !isEmpty(this.cards); }

  render() {
    return (
      <div className={`KanbanColumn -${this.props.kanbanColumn.color}`}>
        <div
          className="header"
        >
          <div className="info">
            <p className="title">{this.props.kanbanColumn.title}</p>
            <p className="total-of-cards">{this.cards.length}</p>
          </div>

          <span
            className="add-new-card"
            onClick={() => this.props.createKanbanCard(this.props.kanbanColumn.id)}
          >
            +
          </span>
        </div>

        <div
          className="body"
          onDrop={(event) => this.whenCardIsDraggedToColumn(event)}
          onDragOver={(event) => this.setElementAsDropzone(event)}
        >
          {this.hasCards ? (
            this.cards.map(kanbanCardId => (
              <KanbanCardContainer
                kanbanCardId={kanbanCardId}
                key={kanbanCardId}
              />
            ))
          ) : (
            // TODO:
            <div className="message">
              <p>No cards :(</p>
              <p>Click "+" to create one!</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}

KanbanColumn.propTypes = {
  kanbanColumn: PropTypes.object,
};

KanbanColumn.defaultProps = {
  color: 'blue',
};
