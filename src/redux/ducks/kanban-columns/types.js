const ENTITY_NAME = 'kanban-columns';

const CREATE_KANBAN_COLUMN = `${ENTITY_NAME}/CREATE_KANBAN_COLUMN`;
const UPDATE_KANBAN_COLUMN = `${ENTITY_NAME}/UPDATE_KANBAN_COLUMN`;

export default {
  ENTITY_NAME,
  CREATE_KANBAN_COLUMN,
  UPDATE_KANBAN_COLUMN,
};
