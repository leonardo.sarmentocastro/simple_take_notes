# simple_take_notes

Kanban board code base for studying purposes (React + Redux).


## Table of contents

1. [Pre requirements](#pre-requirements)
2. [How to run this project](#how-to-run-this-project)
    - [For development](#for-development)
    - [For unit testing](#for-testing)
3. [Continuous integration](#continuous-integration)
4. [Roadmap](#Roadmap)


### 1. Pre requirements

1. Install **Node.js** LTS version. I personally recommend using [nvm](https://github.com/creationix/nvm).

2. Install [yarn](https://yarnpkg.com/en/docs/install).


### 2. How to run this project

Assuming that you have [installed Node.js correctly](#pre-requirements), there follows the commands to start developing and testing your application.

#### For development

Spin up a hot-reloading development server: all changes to the code will be automatically reflected.

```sh
yarn start
```

#### For testing

Runs `jest` test runner on watch mode:

```sh
yarn test
```

To run the tests of a single file, add its name at the end of the command:

```sh
yarn test KanbanBoardPage
```


### 3. Continuous integration

Project repository [can be found here](https://gitlab.com/leonardo.sarmentocastro/simple_take_notes).

Instructions about pipeline stages can be found on [`.gitlab-ci.yml` file](./.gitlab-ci.yml). To deploy, it uses a Firebase. Related files are [`.firebaserc`](./.firebaserc) and [`.firebase.json`](./.firebase.json).

It can be accessed via https://simple-take-notes.firebaseapp.com


### 4. Roadmap

0. Create git tag with commit hash when deploying 
1. Write unit tests for `./redux`;
2. Write unit tests for components/containers with Enzyme;
3. Refactor all `Components` that doesn't use lifecycles to `Functional components`;
4. Maybe use `styled components` since it is a "industry option"? 
5. Add redux-persist;
6. Sketch some integration with GraphQL.
