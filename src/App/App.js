import React, { Component } from 'react';
import PropTypes from 'prop-types';

import '../shared/styles/index.scss';
import './App.styles.scss';

// Responsible for wrapping all presentational related content.
// This way, we can use this same file while using "storybook" for example.
export default class App extends Component {
  render() {
    return (
      <div className="App">
        {this.props.children}
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.any.isRequired,
};
