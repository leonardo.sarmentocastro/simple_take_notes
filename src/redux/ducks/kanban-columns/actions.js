import types from './types';
import reduxUtils from '../../utils';

// NOTE: actions can NORMALIZE data, not mutate them!

const createKanbanColumn = (kanbanColumn) => ({
  type: types.CREATE_KANBAN_COLUMN,
  payload: {
    kanbanColumn: {
      ...kanbanColumn,
      cards: [],
      id: reduxUtils.id(types.ENTITY_NAME),
    }
  }
});

export default {
  createKanbanColumn,
};
