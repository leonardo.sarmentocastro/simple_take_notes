// export reducer as default
import reducer from './reducer';
export default reducer;

// name export actions
export { default as kanbanColumnsActions } from './actions';
export * from './selectors';
