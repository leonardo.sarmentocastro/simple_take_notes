
import types from './types';
import reduxUtils from '../../utils';

// NOTE: actions can NORMALIZE data, not mutate them!

const createKanbanCard = (kanbanColumnId) => ({
  type: types.CREATE_KANBAN_CARD,
  payload: {
    kanbanCard: {
      content: '',
      id: reduxUtils.id(types.ENTITY_NAME),
      kanbanColumn: kanbanColumnId,
    }
  }
});

const deleteKanbanCard = (kanbanCard) => ({
  type: types.DELETE_KANBAN_CARD,
  payload: {
    kanbanCardId: kanbanCard.id,
    kanbanColumnId: kanbanCard.kanbanColumn,
  },
});

const dragKanbanCard = (draggingKanbanCard, toKanbanColumnId) => ({
  type: types.DRAG_KANBAN_CARD,
  payload: { draggingKanbanCard, toKanbanColumnId },
});

// EXAMPLE: actions can NORMALIZE data, not mutate them!
const updateKanbanCard = (kanbanCard, fields) => ({
  type: types.UPDATE_KANBAN_CARD,
  // GOOD: don't need to normalize since will use the whole object
  payload: { kanbanCard, fields },

  // BAD: action is mutate data before passing to reducer
  // payload: {
  //   kanbanCard: {
  //     ...kanbanCard,
  //     ...fields,
  //   },
  // },
});

export default {
  createKanbanCard,
  deleteKanbanCard,
  dragKanbanCard,
  updateKanbanCard,
};
