import uuidv4 from 'uuid/v4';

// creates id with the following format.
const uuid8digits = () => uuidv4().split('-')[0];
const id = (entityName = 'entityName') => `${entityName}.${uuid8digits()}`;

const reduxUtils = { id };
export default reduxUtils;