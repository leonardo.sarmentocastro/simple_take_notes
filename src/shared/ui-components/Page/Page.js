import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Page.scss';

export default class Page extends Component {
  render() {
    return (
      <div className={`Page ${this.props.componentName}`}>
        {this.props.children}
      </div>
    );
  }
}

Page.propTypes = {
  children: PropTypes.any.isRequired,
  componentName: PropTypes.string.isRequired,
};
