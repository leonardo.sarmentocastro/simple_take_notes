import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { kanbanColumnsActions } from '../../../redux/ducks/kanban-columns';
import KanbanBoardPage from '../KanbanBoardPage';

export class KanbanBoardPageContainer extends Component {
  componentDidMount() {
    [
      { title: 'To do', color: 'blue' },
      { title: 'In progress', color: 'red' },
      { title: 'Done!', color: 'submarine' },
    ].forEach(kanbanColumn => this.props.createKanbanColumn(kanbanColumn));
  }

  render() {
    return (
      <KanbanBoardPage
        kanbanColumnIds={this.props.kanbanColumnIds}
      />
    );
  }
}

KanbanBoardPageContainer.propTypes = {
  // redux actions
  createKanbanColumn: PropTypes.func.isRequired,
  // redux state
  kanbanColumnIds: PropTypes.array.isRequired,
};

const mapDispatchToProps = {
  createKanbanColumn: kanbanColumnsActions.createKanbanColumn,
};
const mapStateToProps = ({ kanbanColumns }) => ({
  kanbanColumnIds: kanbanColumns.allIds, // TODO: selector? dunno
});
export default connect(mapStateToProps, mapDispatchToProps)(KanbanBoardPageContainer);
