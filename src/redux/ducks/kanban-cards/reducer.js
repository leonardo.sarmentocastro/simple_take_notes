import types from './types';

// @own-reducers
export const createKanbanCard = (state, action) => {
  const { kanbanCard } = action.payload;

  return {
    byId: {
      ...state.byId,
      [kanbanCard.id]: kanbanCard
    },
    allIds: [
      ...state.allIds,
      kanbanCard.id
    ],
  };
};

// @own-reducers
export const deleteKanbanCard = (state, action) => {
  const { kanbanCardId } = action.payload;

  const { [kanbanCardId]: deletedKanbanCard, ...kanbanCardsById } = state.byId;
  const allIdsFiltered = state.allIds.filter(id => id !== kanbanCardId);

  return {
    byId: { ...kanbanCardsById },
    allIds: [ ...allIdsFiltered ],
  };
}

// @own-reducers
export const dragKanbanCard = (state, action) => {
  const { draggingKanbanCard, toKanbanColumnId } = action.payload;

  // grab latest "kanbanCard" on state so debounced "content" is not overwritten.
  const kanbanCard = state.byId[draggingKanbanCard.id];
  return {
    ...state,
    byId: {
      ...state.byId,
      [draggingKanbanCard.id]: {
        ...kanbanCard,
        kanbanColumn: toKanbanColumnId,
      },
    }
  };
}

// @own-reducers
export const updateKanbanCard = (state, action) => {
  const { kanbanCard, fields } = action.payload;

  return {
    ...state,
    byId: {
      ...state.byId,
      [kanbanCard.id]: {
        ...kanbanCard,
        ...fields,
      }
    },
  };
}

/**
 * STATE SHAPE:

kanbanCards: {
  byId: {
    'kanban-card.1': {
      id: string!,
      content: string!,
      kanbanColumn: ID!,
    },
  },
  allIds: [string!]!
}

*/
const defaultState = {
  byId: {},
  allIds: [],
};
export default function reducer(state = defaultState, action) {
  switch(action.type) {
    // @own-reducers
    case types.CREATE_KANBAN_CARD: return createKanbanCard(state, action);
    case types.DELETE_KANBAN_CARD: return deleteKanbanCard(state, action);
    case types.DRAG_KANBAN_CARD: return dragKanbanCard(state, action);
    case types.UPDATE_KANBAN_CARD: return updateKanbanCard(state, action);


    default: return state;
  }
};
