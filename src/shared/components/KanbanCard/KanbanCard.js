import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './KanbanCard.styles.scss';

export default class KanbanCard extends Component {
  get hasContent() { return this.props.kanbanCard && !!this.props.kanbanCard.content }
  state = {
    content: (this.hasContent ? this.props.kanbanCard.content : ''),
  };

  componentDidUpdate(prevProps) {
    const shouldMirrorRedux = (this.props.kanbanCard !== prevProps.kanbanCard);
    if (shouldMirrorRedux) {
      // Copy redux debounced "content" into component's internal state (100% connected for time-traveling).
      this.setState({ content: this.props.kanbanCard.content });
    }
  }

  setCardOnBrowserDataTransferLayer = (event) => {
    const draggingKanbanCard = this.props.kanbanCard;
    const data = JSON.stringify(draggingKanbanCard);
    event.dataTransfer.setData('text', data);
  }

  setContent = (event) => {
    const content = event.target.value;
    const callback = () => this.props.updateKanbanCard(this.props.kanbanCard, { content });
    this.setState({ content }, callback );
  }

  render() {
    return (
      <div
        id={this.props.id}
        className="KanbanCard"
        draggable="true"
        onDragStart={(event) => this.setCardOnBrowserDataTransferLayer(event)}
      >
        <span
          className="delete-card"
          onClick={() => this.props.deleteIcon.onClick()}
        >
          X
        </span>

        <textarea
          className="content"
          value={this.state.content} // TODO: Time travel doesn't work with this approach.
          onChange={(event) => this.setContent(event)}
        />
      </div>
    );
  }
}

KanbanCard.propTypes = {
  kanbanCard: PropTypes.object,
};
