import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { kanbanCardsActions } from '../../../../redux/ducks/kanban-cards';
import { selectKanbanColumnById } from '../../../../redux/ducks/kanban-columns';
import KanbanColumn from '../KanbanColumn';

export class KanbanColumnContainer extends Component {
  componentDidMount() {
    this.props.createKanbanCard(this.props.kanbanColumnId);
  }

  render() {
    return (
      <KanbanColumn
        createKanbanCard={this.props.createKanbanCard}
        dragKanbanCard={this.props.dragKanbanCard}
        updateKanbanCard={this.props.updateKanbanCard}
        kanbanColumn={this.props.kanbanColumn}
      />
    );
  }
}

KanbanColumnContainer.propTypes = {
  // ownProps
  kanbanColumnId:  PropTypes.string.isRequired,

  // redux actions
  createKanbanCard: PropTypes.func.isRequired,
  dragKanbanCard: PropTypes.func.isRequired,
  updateKanbanCard: PropTypes.func.isRequired,

  // redux store
  kanbanColumn: PropTypes.object.isRequired,
};

// connecting to store
const mapDispatchToProps = {
  createKanbanCard: kanbanCardsActions.createKanbanCard,
  dragKanbanCard: kanbanCardsActions.dragKanbanCard,
  updateKanbanCard: kanbanCardsActions.updateKanbanCard,
};
const mapStateToProps = (state, ownProps) => ({
  kanbanColumn: selectKanbanColumnById(state, ownProps),
});
export default connect(mapStateToProps, mapDispatchToProps)(KanbanColumnContainer);
