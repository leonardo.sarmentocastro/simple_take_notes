import React, { Component } from 'react';
import { Provider as ReduxProvider } from 'react-redux';

import App from '../App';
import { store } from '../../redux';
import { KanbanBoardPageContainer } from '../../pages/KanbanBoardPage';

export class AppContainer extends Component {
  render() {
    return (
      <ReduxProvider store={store}>
        <App>
          <KanbanBoardPageContainer />
        </App>
      </ReduxProvider>
    );
  }
}

export default AppContainer;
