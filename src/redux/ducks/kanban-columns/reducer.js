import types from './types';
import { kanbanCardsTypes } from '../kanban-cards';

// @own-reducers
export const createKanbanColumn = (state, action) => {
  const { kanbanColumn } = action.payload;
  return {
    byId: { ...state.byId, [kanbanColumn.id]: kanbanColumn,  },
    allIds: [ ...state.allIds, kanbanColumn.id ],
  };
}

// @kanban-cards-reducers
export const createKanbanCard = (state, action) => {
  const {
    id: kanbanCardId,
    kanbanColumn: kanbanColumnId
  } = action.payload.kanbanCard;
  const kanbanColumn = state.byId[kanbanColumnId];
  const cards = [ ...kanbanColumn.cards, kanbanCardId ];

  return {
    ...state,
    byId: {
      ...state.byId,
      [kanbanColumnId]: {
        ...kanbanColumn,
        cards,
      }
    }
  };
}

// @kanban-cards-reducers
export const deleteKanbanCard = (state, action) => {
  const { kanbanCardId, kanbanColumnId } = action.payload;
  const kanbanColumn = state.byId[kanbanColumnId];
  const cards = kanbanColumn.cards.filter(card => card !== kanbanCardId);

  return {
    ...state,
    byId: {
      ...state.byId,
      [kanbanColumnId]: {
        ...kanbanColumn,
        cards,
      }
    }
  };
}

// @kanban-cards-reducers
export const dragKanbanCard = (state, action) => {
  const { draggingKanbanCard, toKanbanColumnId } = action.payload;
  const fromKanbanColumn = state.byId[draggingKanbanCard.kanbanColumn];
  const toKanbanColumn = state.byId[toKanbanColumnId];

  return {
    ...state,
    byId: {
      ...state.byId,

      // remove the card from the kanbanColumn its being dragged from
      [fromKanbanColumn.id]: {
        ...fromKanbanColumn,
        cards: fromKanbanColumn.cards.filter(card => card !== draggingKanbanCard.id),
      },

      // add the card to the kanbanColumn its being dragged to
      [toKanbanColumn.id]: {
        ...toKanbanColumn,
        cards: [...toKanbanColumn.cards, draggingKanbanCard.id],
      },
    }
  };
};

/**
 * STATE SHAPE

kanbanColumn: {
  byId: {
    'kanban-column.1': {
      id: string!,
      cards: [],
      color: '',
      title: '',
    },
  },
  allIds: [string!]!
}

*/
const defaultState = {
  byId: {},
  allIds: [],
};
export default function reducer(state = defaultState, action) {
  switch(action.type) {
    // @own-reducers
    case types.CREATE_KANBAN_COLUMN: return createKanbanColumn(state, action);

    // @kanban-cards-reducers
    case kanbanCardsTypes.CREATE_KANBAN_CARD: return createKanbanCard(state, action);
    case kanbanCardsTypes.DELETE_KANBAN_CARD: return deleteKanbanCard(state, action);
    case kanbanCardsTypes.DRAG_KANBAN_CARD: return dragKanbanCard(state, action);

    default: return state;
  }
}
