export const selectKanbanCardById = (state, ownProps) => state.kanbanCards.byId[ownProps.kanbanCardId];
