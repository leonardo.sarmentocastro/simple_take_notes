import React, { Component } from 'react';
import debounce from 'lodash/debounce';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { kanbanCardsActions, selectKanbanCardById } from '../../../../redux/ducks/kanban-cards';
import KanbanCard from '../KanbanCard';

export class KanbanCardContainer extends Component {
  debouncedUpdateKanbanCard = debounce(this.props.updateKanbanCard, 500);

  render() {
    return (
      <KanbanCard
        // redux actions
        deleteIcon={{ onClick: () => this.props.deleteKanbanCard(this.props.kanbanCard) }}
        updateKanbanCard={this.debouncedUpdateKanbanCard}

        // redux store
        kanbanCard={this.props.kanbanCard}
      />
    );
  }
}

KanbanCardContainer.propTypes = {
  // redux actions
  deleteKanbanCard: PropTypes.func.isRequired,
  updateKanbanCard: PropTypes.func.isRequired,

  // redux store
  kanbanCard: PropTypes.object.isRequired,
};
const mapDispatchToProps = {
  deleteKanbanCard: kanbanCardsActions.deleteKanbanCard,
  updateKanbanCard: kanbanCardsActions.updateKanbanCard,
};
const mapStateToProps = (state, ownProps) => ({
  kanbanCard: selectKanbanCardById(state, ownProps),
});
export default connect(mapStateToProps, mapDispatchToProps)(KanbanCardContainer);
