import React from 'react';
import PropTypes from 'prop-types';

import { KanbanColumnContainer } from '../../shared/components';
import { Page } from '../../shared/ui-components';
import './KanbanBoardPage.styles.scss';

export default function KanbanBoardPage({ kanbanColumnIds }) {
  return (
    <Page componentName="KanbanBoardPage">
      {kanbanColumnIds.map(kanbanColumnId => (
        <KanbanColumnContainer
          key={kanbanColumnId}
          kanbanColumnId={kanbanColumnId}
        />
      ))}
    </Page>
  );
}

KanbanBoardPage.propTypes = {
  kanbanColumnIds: PropTypes.array.isRequired,
};
